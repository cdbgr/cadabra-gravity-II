{
	"cell_id": 10941041495556452075,
	"cells": [
		{
			"cell_id": 16277287802720446119,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 1941243910648839111,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\part*{Gravitational Waves Relations}\n\\author{Oscar Castillo-Felisola, Dominic T. Price and Mattia Scomparin}\n\\email{mattia.scompa@gmail.com}\n\nWorking in the harmonic gauge, in this notebook we consider plane-wave solutions of the\nwave-equation which describes the propagation of gravitational waves in the absence of matter fields. We\nshow that such solutions must satisfy the well-known wave-relations. This tutorial is useful for showing\nthe versatility of \\algo{cadabra} in managing tensor expressions that belong to the argument of exponential\nfunctions."
				}
			],
			"hidden": true,
			"source": "\\part*{Gravitational Waves Relations}\n\\author{Oscar Castillo-Felisola, Dominic T. Price and Mattia Scomparin}\n\\email{mattia.scompa@gmail.com}\n\nWorking in the harmonic gauge, in this notebook we consider plane-wave solutions of the\nwave-equation which describes the propagation of gravitational waves in the absence of matter fields. We\nshow that such solutions must satisfy the well-known wave-relations. This tutorial is useful for showing\nthe versatility of \\algo{cadabra} in managing tensor expressions that belong to the argument of exponential\nfunctions."
		},
		{
			"cell_id": 8966640604315083725,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 17919676517381375689,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Definitions and modules}\nAs with the notebook from the previous section we begin by importing the results of section 4, on top of which we import\ntwo functions from the \\verb|cadabra| standard library: \\verb|replace_index| which renames all indices in a subexpression,\nand \\verb|get_basis_component| which extracts the coefficient of a term."
				}
			],
			"hidden": true,
			"source": "\\section*{Definitions and modules}\nAs with the notebook from the previous section we begin by importing the results of section 4, on top of which we import\ntwo functions from the \\verb|cadabra| standard library: \\verb|replace_index| which renames all indices in a subexpression,\nand \\verb|get_basis_component| which extracts the coefficient of a term."
		},
		{
			"cell_id": 5059869152097514233,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "from sec4_TensorPerturbationsGR import *\nfrom cdb.core.manip import get_basis_component\nfrom cdb.utils.indices import replace_index"
		},
		{
			"cell_id": 13596573199768113439,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 655659898961745571,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Definitions}"
				}
			],
			"hidden": true,
			"source": "\\section*{Definitions}"
		},
		{
			"cell_id": 11980203391136657332,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 13232368489399121984,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "We introduce the spacetime coordinate $x^\\mu$"
				}
			],
			"hidden": true,
			"source": "We introduce the spacetime coordinate $x^\\mu$"
		},
		{
			"cell_id": 6359372077945682868,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "x::Coordinate.\nx{#}::Depends(\\partial{#})."
		},
		{
			"cell_id": 18195688870173743102,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 7273915200680007867,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "as well as the wave-number $k_\\lambda$, the polarization tensor $\\mathbf{e}_{\\mu\\nu}$ and its complex conjugate\n$\\bar{\\mathbf{e}}_{\\mu\\nu}$"
				}
			],
			"hidden": true,
			"source": "as well as the wave-number $k_\\lambda$, the polarization tensor $\\mathbf{e}_{\\mu\\nu}$ and its complex conjugate\n$\\bar{\\mathbf{e}}_{\\mu\\nu}$"
		},
		{
			"cell_id": 15879836157580363258,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "\\pol1{#}::LaTeXForm(\"\\mathbf{e}\").\n\\pol2{#}::LaTeXForm(\"\\bar{\\mathbf{e}}\")."
		},
		{
			"cell_id": 4379256039936529657,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 9918541153423227380,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "Now we can consider the plane-wave parametrization"
				}
			],
			"hidden": true,
			"source": "Now we can consider the plane-wave parametrization"
		},
		{
			"cell_id": 4627082961981465212,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 9996835745827777456,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 12272651012043722308,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "h1_{\\mu \\nu} = \\pol1_{\\mu \\nu} \\exp(i k_{\\lambda} x^{\\lambda}) + \\pol2_{\\mu \\nu} \\exp(-i k_{\\lambda} x^{\\lambda})"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(1)}}h_{\\mu \\nu} = \\mathbf{e}_{\\mu \\nu} \\exp\\left(i k_{\\lambda} x^{\\lambda}\\right)+\\bar{\\mathbf{e}}_{\\mu \\nu} \\exp\\left(-i k_{\\lambda} x^{\\lambda}\\right)\\end{dmath*}"
				}
			],
			"source": "sol := h1_{\\mu\\nu} = \\pol1_{\\mu\\nu} \\exp(i*k_\\lambda*x^{\\lambda})+ \\pol2_{\\mu\\nu} \\exp(-i*k_\\lambda*x^{\\lambda});"
		},
		{
			"cell_id": 14901810042390322023,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 11049271284634264594,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "where $i$ is the imaginary unit. Here $h_{\\mu\\nu}$ is symmetric and inherits its dependence on the derivative $\\partial_\\mu$ \nthrough $\\exp(\\pm i k_\\lambda x^{\\lambda})$"
				}
			],
			"hidden": true,
			"source": "where $i$ is the imaginary unit. Here $h_{\\mu\\nu}$ is symmetric and inherits its dependence on the derivative $\\partial_\\mu$ \nthrough $\\exp(\\pm i k_\\lambda x^{\\lambda})$"
		},
		{
			"cell_id": 10667226512881200930,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "\\exp(i*k_\\lambda*x{#})::Depends(\\partial{#}).\n\\exp(-i*k_\\lambda*x{#})::Depends(\\partial{#})."
		},
		{
			"cell_id": 18067779059717551956,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 443959826710291141,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "For the purposes of our analysis, the physics is described by the vacuum wave-equation"
				}
			],
			"hidden": true,
			"source": "For the purposes of our analysis, the physics is described by the vacuum wave-equation"
		},
		{
			"cell_id": 17644527784953946195,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 11691056036142920411,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 3566654404721845100,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\partial_{\\sigma}^{\\sigma}(h1_{\\mu \\nu}) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\partial_{\\sigma}\\,^{\\sigma}{\\,^{^{(1)}}h_{\\mu \\nu}} = 0\\end{dmath*}"
				}
			],
			"source": "wave_eq := \\partial_{\\sigma}{\\partial^{\\sigma}{h1_{\\mu \\nu}}} = 0;"
		},
		{
			"cell_id": 8812480122381007163,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 6937483337487778329,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "and by the harmonic gauge condition"
				}
			],
			"hidden": true,
			"source": "and by the harmonic gauge condition"
		},
		{
			"cell_id": 12265984358363976812,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 3616207241261036952,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 13698222341737939014,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\partial_{\\mu}(h1^{\\mu}_{\\nu}) -  1/2 \\partial_{\\nu}(h1^{\\mu}_{\\mu}) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\partial_{\\mu}{\\,^{^{(1)}}h^{\\mu}\\,_{\\nu}} - \\frac{1}{2}\\partial_{\\nu}{\\,^{^{(1)}}h^{\\mu}\\,_{\\mu}} = 0\\end{dmath*}"
				}
			],
			"source": "gauge_eq := \\partial_{\\mu}{h1^{\\mu}_{\\nu}}} - 1/2*\\partial_{\\nu}{h1^{\\mu}_{\\mu}} = 0;"
		},
		{
			"cell_id": 15625183976664328768,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 15316237914403728142,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "More details on obtaining the wave-equation and the harmonic gauge first-order decomposition\ncan be found in the previous tutorials. \\\\\nIn order to handle the derivatives of the exponential function $\\exp(\\pm i k_\\lambda x^{\\lambda})$, \nthe following replacement rules are required:"
				}
			],
			"hidden": true,
			"source": "More details on obtaining the wave-equation and the harmonic gauge first-order decomposition\ncan be found in the previous tutorials. \\\\\nIn order to handle the derivatives of the exponential function $\\exp(\\pm i k_\\lambda x^{\\lambda})$, \nthe following replacement rules are required:"
		},
		{
			"cell_id": 10476020106172432566,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 2719763630242983941,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 13063283204412373652,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\partial_{\\sigma}^{\\sigma}(\\exp(A??)) -> \\partial_{\\sigma}(\\exp(A??) \\partial^{\\sigma}(A??))"
						}
					],
					"source": "\\begin{dmath*}{}\\partial_{\\sigma}\\,^{\\sigma}\\left(\\exp\\left(A??\\right)\\right) \\rightarrow \\partial_{\\sigma}\\left(\\exp\\left(A??\\right) \\partial^{\\sigma}\\left(A??\\right)\\right)\\end{dmath*}"
				},
				{
					"cell_id": 1233009613135593368,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 7613007765825228099,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\partial^{\\mu}(\\exp(A??)) -> \\exp(A??) \\partial^{\\mu}(A??)"
						}
					],
					"source": "\\begin{dmath*}{}\\partial^{\\mu}\\left(\\exp\\left(A??\\right)\\right) \\rightarrow \\exp\\left(A??\\right) \\partial^{\\mu}\\left(A??\\right)\\end{dmath*}"
				},
				{
					"cell_id": 11948944010149657697,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 3512601608473229755,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\partial_{\\mu}(\\exp(A??)) -> \\exp(A??) \\partial_{\\mu}(A??)"
						}
					],
					"source": "\\begin{dmath*}{}\\partial_{\\mu}\\left(\\exp\\left(A??\\right)\\right) \\rightarrow \\exp\\left(A??\\right) \\partial_{\\mu}\\left(A??\\right)\\end{dmath*}"
				}
			],
			"source": "rule_1 := \\partial_{\\sigma}^{\\sigma}(\\exp(A??)) -> \\partial_{\\sigma}{\\exp(A??)*\\partial^{\\sigma}{A??}};\nrule_2 := \\partial^{\\mu}{\\exp(A??)} -> \\exp(A??)*\\partial^{\\mu}{A??};\nrule_3 := \\partial_{\\mu}{\\exp(A??)} -> \\exp(A??)*\\partial_{\\mu}{A??};"
		},
		{
			"cell_id": 15081374878483291887,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 14540391995618256989,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{First wave-relation}\n\nWe begin by imposing that the plane-wave $h_{\\mu\\nu}$ is a solution of the vacuum wave equation"
				}
			],
			"hidden": true,
			"source": "\\section*{First wave-relation}\n\nWe begin by imposing that the plane-wave $h_{\\mu\\nu}$ is a solution of the vacuum wave equation"
		},
		{
			"cell_id": 520509155381319404,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 9528228366654179270,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 17194152637798896709,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\partial_{\\sigma}^{\\sigma}(h1_{\\mu \\nu}) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\partial_{\\sigma}\\,^{\\sigma}{\\,^{^{(1)}}h_{\\mu \\nu}} = 0\\end{dmath*}"
				},
				{
					"cell_id": 275785936995287225,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 10424379404752115362,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\partial_{\\sigma}^{\\sigma}((\\pol1_{\\mu \\nu} \\exp(i k_{\\lambda} x^{\\lambda}) + \\pol2_{\\mu \\nu} \\exp(-i k_{\\lambda} x^{\\lambda}))) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\partial_{\\sigma}\\,^{\\sigma}\\left(\\mathbf{e}_{\\mu \\nu} \\exp\\left(i k_{\\lambda} x^{\\lambda}\\right)+\\bar{\\mathbf{e}}_{\\mu \\nu} \\exp\\left(-i k_{\\lambda} x^{\\lambda}\\right)\\right) = 0\\end{dmath*}"
				}
			],
			"source": "wave_eq1 := @(wave_eq);\nsubstitute(_, sol);"
		},
		{
			"cell_id": 6965374141328707545,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 16096326643435264135,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "To simplify the expression, we need to use the Cadabra specific \\verb|converge| construction, which is essentially a while\nloop which runs until the predicate expression no longer changes"
				}
			],
			"hidden": true,
			"source": "To simplify the expression, we need to use the Cadabra specific \\verb|converge| construction, which is essentially a while\nloop which runs until the predicate expression no longer changes"
		},
		{
			"cell_id": 1727419695893897728,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 12592130903315530355,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 4577183879135053615,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\pol1_{\\mu \\nu} \\partial_{\\sigma}^{\\sigma}(\\exp(i k_{\\lambda} x^{\\lambda})) + \\pol2_{\\mu \\nu} \\partial_{\\sigma}^{\\sigma}(\\exp(-i k_{\\lambda} x^{\\lambda})) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\mathbf{e}_{\\mu \\nu} \\partial_{\\sigma}\\,^{\\sigma}\\left(\\exp\\left(i k_{\\lambda} x^{\\lambda}\\right)\\right)+\\bar{\\mathbf{e}}_{\\mu \\nu} \\partial_{\\sigma}\\,^{\\sigma}\\left(\\exp\\left(-i k_{\\lambda} x^{\\lambda}\\right)\\right) = 0\\end{dmath*}"
				}
			],
			"source": "converge(wave_eq1):\n\tdistribute(_)\n\tproduct_rule(_)\n\tunwrap(_)\nwave_eq1;"
		},
		{
			"cell_id": 1302160180337584930,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 15243979547765638479,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "Note that $\\partial_\\sigma^{~\\sigma} = \\partial_\\sigma \\partial^\\sigma$. In order to calculate the derivative \n$\\partial^{\\sigma}$ we use substitute in \\verb|rule_1| from above and use the \\verb|replace_index| algorithm \nimported at the beginning to ensure that all the dummy pairs have unique names"
				}
			],
			"hidden": true,
			"source": "Note that $\\partial_\\sigma^{~\\sigma} = \\partial_\\sigma \\partial^\\sigma$. In order to calculate the derivative \n$\\partial^{\\sigma}$ we use substitute in \\verb|rule_1| from above and use the \\verb|replace_index| algorithm \nimported at the beginning to ensure that all the dummy pairs have unique names"
		},
		{
			"cell_id": 14789242911013936197,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 17867898292261471579,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 11046630174577910999,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\pol1_{\\mu \\nu} \\partial_{\\sigma}(\\exp(i k_{\\psi} x^{\\psi}) \\partial^{\\sigma}(i k_{\\lambda} x^{\\lambda}))-\\pol2_{\\mu \\nu} \\partial_{\\sigma}(\\exp(-i k_{\\psi} x^{\\psi}) \\partial^{\\sigma}(i k_{\\lambda} x^{\\lambda})) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\mathbf{e}_{\\mu \\nu} \\partial_{\\sigma}\\left(\\exp\\left(i k_{\\psi} x^{\\psi}\\right) \\partial^{\\sigma}\\left(i k_{\\lambda} x^{\\lambda}\\right)\\right)-\\bar{\\mathbf{e}}_{\\mu \\nu} \\partial_{\\sigma}\\left(\\exp\\left(-i k_{\\psi} x^{\\psi}\\right) \\partial^{\\sigma}\\left(i k_{\\lambda} x^{\\lambda}\\right)\\right) = 0\\end{dmath*}"
				}
			],
			"source": "substitute(_, rule_1)\nreplace_index(_, r'\\exp', r'\\lambda', r'\\psi');"
		},
		{
			"cell_id": 11745206536777709732,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 17170105708687938202,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "We now use the \\verb|unwrap| algorithm to take the constants out of the derivative, substitute in the metric where\nthere are derivatives of the coordinate (i.e. $\\partial^{\\mu}{x^{\\alpha}} \\rightarrow \\eta^{\\mu\\alpha}$) and then perform\nanother \\verb|unwrap|."
				}
			],
			"hidden": true,
			"source": "We now use the \\verb|unwrap| algorithm to take the constants out of the derivative, substitute in the metric where\nthere are derivatives of the coordinate (i.e. $\\partial^{\\mu}{x^{\\alpha}} \\rightarrow \\eta^{\\mu\\alpha}$) and then perform\nanother \\verb|unwrap|."
		},
		{
			"cell_id": 7624824605680390003,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 14738698208263715826,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 2354673916187730169,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\pol1_{\\mu \\nu} i k_{\\lambda} \\partial_{\\sigma}(\\exp(i k_{\\psi} x^{\\psi}) \\eta^{\\sigma \\lambda})-\\pol2_{\\mu \\nu} i k_{\\lambda} \\partial_{\\sigma}(\\exp(-i k_{\\psi} x^{\\psi}) \\eta^{\\sigma \\lambda}) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\mathbf{e}_{\\mu \\nu} i k_{\\lambda} \\partial_{\\sigma}\\left(\\exp\\left(i k_{\\psi} x^{\\psi}\\right) \\eta^{\\sigma \\lambda}\\right)-\\bar{\\mathbf{e}}_{\\mu \\nu} i k_{\\lambda} \\partial_{\\sigma}\\left(\\exp\\left(-i k_{\\psi} x^{\\psi}\\right) \\eta^{\\sigma \\lambda}\\right) = 0\\end{dmath*}"
				},
				{
					"cell_id": 16724934964329814510,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 5638877128674672720,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\pol1_{\\mu \\nu} i k_{\\lambda} \\eta^{\\sigma \\lambda} \\partial_{\\sigma}(\\exp(i k_{\\psi} x^{\\psi}))-\\pol2_{\\mu \\nu} i k_{\\lambda} \\eta^{\\sigma \\lambda} \\partial_{\\sigma}(\\exp(-i k_{\\psi} x^{\\psi})) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\mathbf{e}_{\\mu \\nu} i k_{\\lambda} \\eta^{\\sigma \\lambda} \\partial_{\\sigma}\\left(\\exp\\left(i k_{\\psi} x^{\\psi}\\right)\\right)-\\bar{\\mathbf{e}}_{\\mu \\nu} i k_{\\lambda} \\eta^{\\sigma \\lambda} \\partial_{\\sigma}\\left(\\exp\\left(-i k_{\\psi} x^{\\psi}\\right)\\right) = 0\\end{dmath*}"
				}
			],
			"source": "unwrap(_)\nsubstitute(_, $\\partial^{\\mu}{x^{\\alpha}} -> \\eta^{\\mu\\alpha}$);\nunwrap(_);"
		},
		{
			"cell_id": 3151710713894502913,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 8364635777773600542,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "We now calculate the derivative of the exponentional using \\verb|rule_3| and do another dummy index substitution\nas above"
				}
			],
			"hidden": true,
			"source": "We now calculate the derivative of the exponentional using \\verb|rule_3| and do another dummy index substitution\nas above"
		},
		{
			"cell_id": 14572388657461495018,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 9795240137445515824,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 5807908389789135864,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\pol1_{\\mu \\nu} i k_{\\lambda} \\eta^{\\sigma \\lambda} \\exp(i k_{\\pi} x^{\\pi}) \\partial_{\\sigma}(i k_{\\psi} x^{\\psi}) + \\pol2_{\\mu \\nu} i k_{\\lambda} \\eta^{\\sigma \\lambda} \\exp(-i k_{\\pi} x^{\\pi}) \\partial_{\\sigma}(i k_{\\psi} x^{\\psi}) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\mathbf{e}_{\\mu \\nu} i k_{\\lambda} \\eta^{\\sigma \\lambda} \\exp\\left(i k_{\\pi} x^{\\pi}\\right) \\partial_{\\sigma}\\left(i k_{\\psi} x^{\\psi}\\right)+\\bar{\\mathbf{e}}_{\\mu \\nu} i k_{\\lambda} \\eta^{\\sigma \\lambda} \\exp\\left(-i k_{\\pi} x^{\\pi}\\right) \\partial_{\\sigma}\\left(i k_{\\psi} x^{\\psi}\\right) = 0\\end{dmath*}"
				}
			],
			"source": "substitute(_, rule_3)\nreplace_index(_, r'\\exp', r'\\psi', r'\\pi');"
		},
		{
			"cell_id": 7929786503167533699,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 6283918618987145654,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "To finish off this calculation we once more use \\verb|unwrap| to take out the constants before performing the substitution\n$\\partial_{\\mu}{x^{\\alpha}} \\rightarrow \\delta_{\\mu}^{~\\alpha}$ and cleaning up the result"
				}
			],
			"hidden": true,
			"source": "To finish off this calculation we once more use \\verb|unwrap| to take out the constants before performing the substitution\n$\\partial_{\\mu}{x^{\\alpha}} \\rightarrow \\delta_{\\mu}^{~\\alpha}$ and cleaning up the result"
		},
		{
			"cell_id": 16168401636624546310,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 12539271066073256988,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 11313421836683553758,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "-\\pol1_{\\mu \\nu} k_{\\lambda} \\eta^{\\sigma \\lambda} \\exp(i k_{\\pi} x^{\\pi}) k_{\\psi} \\partial_{\\sigma}(x^{\\psi})-\\pol2_{\\mu \\nu} k_{\\lambda} \\eta^{\\sigma \\lambda} \\exp(-i k_{\\pi} x^{\\pi}) k_{\\psi} \\partial_{\\sigma}(x^{\\psi}) = 0"
						}
					],
					"source": "\\begin{dmath*}{}-\\mathbf{e}_{\\mu \\nu} k_{\\lambda} \\eta^{\\sigma \\lambda} \\exp\\left(i k_{\\pi} x^{\\pi}\\right) k_{\\psi} \\partial_{\\sigma}{x^{\\psi}}-\\bar{\\mathbf{e}}_{\\mu \\nu} k_{\\lambda} \\eta^{\\sigma \\lambda} \\exp\\left(-i k_{\\pi} x^{\\pi}\\right) k_{\\psi} \\partial_{\\sigma}{x^{\\psi}} = 0\\end{dmath*}"
				},
				{
					"cell_id": 9991258296677157975,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 14028600725687106064,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "-\\pol1_{\\mu \\nu} k_{\\lambda} \\eta^{\\psi \\lambda} \\exp(i k_{\\pi} x^{\\pi}) k_{\\psi}-\\pol2_{\\mu \\nu} k_{\\lambda} \\eta^{\\psi \\lambda} \\exp(-i k_{\\pi} x^{\\pi}) k_{\\psi} = 0"
						}
					],
					"source": "\\begin{dmath*}{}-\\mathbf{e}_{\\mu \\nu} k_{\\lambda} \\eta^{\\psi \\lambda} \\exp\\left(i k_{\\pi} x^{\\pi}\\right) k_{\\psi}-\\bar{\\mathbf{e}}_{\\mu \\nu} k_{\\lambda} \\eta^{\\psi \\lambda} \\exp\\left(-i k_{\\pi} x^{\\pi}\\right) k_{\\psi} = 0\\end{dmath*}"
				},
				{
					"cell_id": 5237342707238535423,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 884806328544027397,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\exp(i k_{\\pi} x^{\\pi}) \\pol1_{\\mu \\nu} k_{\\lambda} k^{\\lambda} + \\exp(-i k_{\\pi} x^{\\pi}) \\pol2_{\\mu \\nu} k_{\\lambda} k^{\\lambda} = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\exp\\left(i k_{\\pi} x^{\\pi}\\right) \\mathbf{e}_{\\mu \\nu} k_{\\lambda} k^{\\lambda}+\\exp\\left(-i k_{\\pi} x^{\\pi}\\right) \\bar{\\mathbf{e}}_{\\mu \\nu} k_{\\lambda} k^{\\lambda} = 0\\end{dmath*}"
				}
			],
			"source": "unwrap(_);\nsubstitute(_, $\\partial_{\\mu}{x^{\\alpha}} -> \\delta_{\\mu}^{\\alpha}$) # Can also do \\partial_{\\mu}{x^{\\alpha}}:KroneckerDelta\neliminate_kronecker(_);\neliminate_metric(_)\nsort_product(_)\nmanip.multiply_through(wave_eq1, $-1$);"
		},
		{
			"cell_id": 17200635490112827084,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 6295326695652414338,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "This equation must be true for every value of $\\mathbf{e}_{\\mu\\nu}$ and $\\bar{\\mathbf{e}}_{\\mu\\nu}$. \nConsidering for example $\\mathbf{e}_{\\mu\\nu}$, the first wave relation can be extracted using the \n\\verb|get_basis_component| algorithm"
				}
			],
			"hidden": true,
			"source": "This equation must be true for every value of $\\mathbf{e}_{\\mu\\nu}$ and $\\bar{\\mathbf{e}}_{\\mu\\nu}$. \nConsidering for example $\\mathbf{e}_{\\mu\\nu}$, the first wave relation can be extracted using the \n\\verb|get_basis_component| algorithm"
		},
		{
			"cell_id": 16694496166950329465,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 4968384015753941623,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 9616135540844484767,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "k_{\\lambda} k^{\\lambda} = 0"
						}
					],
					"source": "\\begin{dmath*}{}k_{\\lambda} k^{\\lambda} = 0\\end{dmath*}"
				}
			],
			"source": "wave_eq1 = get_basis_component(wave_eq1 ,$\\pol1{#}\\exp(A??)$);"
		},
		{
			"cell_id": 17196252872930452871,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 4361706059664902175,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "Clearly, we would have achieved the same result using \\verb|\\pol2{#}\\exp(A??)| as the basis."
				}
			],
			"hidden": true,
			"source": "Clearly, we would have achieved the same result using \\verb|\\pol2{#}\\exp(A??)| as the basis."
		},
		{
			"cell_id": 14119219067944453401,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 12178974446448070356,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Second wave-relation}\n\nAs $h_{\\mu\\nu}$ is a small perturbation around the flat spacetime, its indices can been raised or lowered using the\nMinkowski metric $\\eta_{\\mu\\nu}$"
				}
			],
			"hidden": true,
			"source": "\\section*{Second wave-relation}\n\nAs $h_{\\mu\\nu}$ is a small perturbation around the flat spacetime, its indices can been raised or lowered using the\nMinkowski metric $\\eta_{\\mu\\nu}$"
		},
		{
			"cell_id": 8410044224545749567,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 4512907162597322504,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 16335778848848567645,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "h1_{\\mu \\nu} = \\pol1_{\\mu \\nu} \\exp(i k_{\\lambda} x^{\\lambda}) + \\pol2_{\\mu \\nu} \\exp(-i k_{\\lambda} x^{\\lambda})"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(1)}}h_{\\mu \\nu} = \\mathbf{e}_{\\mu \\nu} \\exp\\left(i k_{\\lambda} x^{\\lambda}\\right)+\\bar{\\mathbf{e}}_{\\mu \\nu} \\exp\\left(-i k_{\\lambda} x^{\\lambda}\\right)\\end{dmath*}"
				},
				{
					"cell_id": 14951907293697346607,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 10847173875397103284,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "h1^{\\gamma}_{\\nu} = \\pol1^{\\gamma}_{\\nu} \\exp(i k_{\\lambda} x^{\\lambda}) + \\pol2^{\\gamma}_{\\nu} \\exp(-i k_{\\lambda} x^{\\lambda})"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(1)}}h^{\\gamma}\\,_{\\nu} = \\mathbf{e}^{\\gamma}\\,_{\\nu} \\exp\\left(i k_{\\lambda} x^{\\lambda}\\right)+\\bar{\\mathbf{e}}^{\\gamma}\\,_{\\nu} \\exp\\left(-i k_{\\lambda} x^{\\lambda}\\right)\\end{dmath*}"
				}
			],
			"source": "sol2 := @(sol);\nmanip.multiply_through(_, $\\eta^{\\gamma\\mu}$)\ndistribute(_)\neliminate_metric(_);"
		},
		{
			"cell_id": 18373878133560120986,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 1563163310263624006,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "Let us impose that the plane-wave $h^\\gamma_{~\\nu}$ satisfies the harmonic-gauge condition and use \\verb|distribute| and\n\\verb|unwrap| to move the constants out of the derivative"
				}
			],
			"hidden": true,
			"source": "Let us impose that the plane-wave $h^\\gamma_{~\\nu}$ satisfies the harmonic-gauge condition and use \\verb|distribute| and\n\\verb|unwrap| to move the constants out of the derivative"
		},
		{
			"cell_id": 6261291063962998662,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 17493735486054814558,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 3270904243986879900,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\partial_{\\mu}(h1^{\\mu}_{\\nu}) -  1/2 \\partial_{\\nu}(h1^{\\mu}_{\\mu}) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\partial_{\\mu}{\\,^{^{(1)}}h^{\\mu}\\,_{\\nu}} - \\frac{1}{2}\\partial_{\\nu}{\\,^{^{(1)}}h^{\\mu}\\,_{\\mu}} = 0\\end{dmath*}"
				},
				{
					"cell_id": 12844908236802959949,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 179564004427006130,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\partial_{\\mu}(\\pol1^{\\mu}_{\\nu} \\exp(i k_{\\lambda} x^{\\lambda})) + \\partial_{\\mu}(\\pol2^{\\mu}_{\\nu} \\exp(-i k_{\\lambda} x^{\\lambda})) -  1/2 \\partial_{\\nu}(\\pol1^{\\mu}_{\\mu} \\exp(i k_{\\lambda} x^{\\lambda})) -  1/2 \\partial_{\\nu}(\\pol2^{\\mu}_{\\mu} \\exp(-i k_{\\lambda} x^{\\lambda})) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\partial_{\\mu}\\left(\\mathbf{e}^{\\mu}\\,_{\\nu} \\exp\\left(i k_{\\lambda} x^{\\lambda}\\right)\\right)+\\partial_{\\mu}\\left(\\bar{\\mathbf{e}}^{\\mu}\\,_{\\nu} \\exp\\left(-i k_{\\lambda} x^{\\lambda}\\right)\\right) - \\frac{1}{2}\\partial_{\\nu}\\left(\\mathbf{e}^{\\mu}\\,_{\\mu} \\exp\\left(i k_{\\lambda} x^{\\lambda}\\right)\\right) - \\frac{1}{2}\\partial_{\\nu}\\left(\\bar{\\mathbf{e}}^{\\mu}\\,_{\\mu} \\exp\\left(-i k_{\\lambda} x^{\\lambda}\\right)\\right) = 0\\end{dmath*}"
				}
			],
			"source": "wave_eq2 := @(gauge_eq);\nsubstitute(_, sol2)\ndistribute(_);"
		},
		{
			"cell_id": 13330458407713689008,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 11577153421040754314,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "As with the the first wave-relation, we use \\verb|unwrap| to move the constants outside of the derivative,\nsubstitute in \\verb|rule_3| to calculate the derivative before cleaning up the indices with \\verb|replace_index|\nand calling \\verb|unwrap| again"
				}
			],
			"hidden": true,
			"source": "As with the the first wave-relation, we use \\verb|unwrap| to move the constants outside of the derivative,\nsubstitute in \\verb|rule_3| to calculate the derivative before cleaning up the indices with \\verb|replace_index|\nand calling \\verb|unwrap| again"
		},
		{
			"cell_id": 17439184963870922846,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 582088419063478382,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 4758316537190279312,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\pol1^{\\mu}_{\\nu} \\exp(i k_{\\psi} x^{\\psi}) i k_{\\lambda} \\partial_{\\mu}(x^{\\lambda})-\\pol2^{\\mu}_{\\nu} \\exp(-i k_{\\psi} x^{\\psi}) i k_{\\lambda} \\partial_{\\mu}(x^{\\lambda}) -  1/2 \\pol1^{\\mu}_{\\mu} \\exp(i k_{\\psi} x^{\\psi}) i k_{\\lambda} \\partial_{\\nu}(x^{\\lambda}) +  1/2 \\pol2^{\\mu}_{\\mu} \\exp(-i k_{\\psi} x^{\\psi}) i k_{\\lambda} \\partial_{\\nu}(x^{\\lambda}) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\mathbf{e}^{\\mu}\\,_{\\nu} \\exp\\left(i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda} \\partial_{\\mu}{x^{\\lambda}}-\\bar{\\mathbf{e}}^{\\mu}\\,_{\\nu} \\exp\\left(-i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda} \\partial_{\\mu}{x^{\\lambda}} - \\frac{1}{2}\\mathbf{e}^{\\mu}\\,_{\\mu} \\exp\\left(i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda} \\partial_{\\nu}{x^{\\lambda}}+\\frac{1}{2}\\bar{\\mathbf{e}}^{\\mu}\\,_{\\mu} \\exp\\left(-i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda} \\partial_{\\nu}{x^{\\lambda}} = 0\\end{dmath*}"
				}
			],
			"source": "unwrap(_)\nsubstitute(_, rule_3)\nreplace_index(_, r'\\exp', r'\\lambda', r'\\psi')\nunwrap(_);"
		},
		{
			"cell_id": 9441829341929329386,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 11400362570164596733,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "As before we replace $\\partial_{\\mu}{x^{\\lambda}}$ with the Kronecker delta which we then resolve with \n\\verb|eliminate_kronecker|. We then separate out the coefficients of the two different exponential functions\nusing \\verb|factor_out|"
				}
			],
			"hidden": true,
			"source": "As before we replace $\\partial_{\\mu}{x^{\\lambda}}$ with the Kronecker delta which we then resolve with \n\\verb|eliminate_kronecker|. We then separate out the coefficients of the two different exponential functions\nusing \\verb|factor_out|"
		},
		{
			"cell_id": 13469145867259520678,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 15022155417746399320,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 12711559785951929243,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\pol1^{\\mu}_{\\nu} \\exp(i k_{\\psi} x^{\\psi}) i k_{\\lambda} \\delta_{\\mu}^{\\lambda}-\\pol2^{\\mu}_{\\nu} \\exp(-i k_{\\psi} x^{\\psi}) i k_{\\lambda} \\delta_{\\mu}^{\\lambda} -  1/2 \\pol1^{\\mu}_{\\mu} \\exp(i k_{\\psi} x^{\\psi}) i k_{\\lambda} \\delta_{\\nu}^{\\lambda} +  1/2 \\pol2^{\\mu}_{\\mu} \\exp(-i k_{\\psi} x^{\\psi}) i k_{\\lambda} \\delta_{\\nu}^{\\lambda} = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\mathbf{e}^{\\mu}\\,_{\\nu} \\exp\\left(i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda} \\delta_{\\mu}\\,^{\\lambda}-\\bar{\\mathbf{e}}^{\\mu}\\,_{\\nu} \\exp\\left(-i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda} \\delta_{\\mu}\\,^{\\lambda} - \\frac{1}{2}\\mathbf{e}^{\\mu}\\,_{\\mu} \\exp\\left(i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda} \\delta_{\\nu}\\,^{\\lambda}+\\frac{1}{2}\\bar{\\mathbf{e}}^{\\mu}\\,_{\\mu} \\exp\\left(-i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda} \\delta_{\\nu}\\,^{\\lambda} = 0\\end{dmath*}"
				},
				{
					"cell_id": 10499821355143284216,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 11987045523030912468,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\pol1^{\\lambda}_{\\nu} \\exp(i k_{\\psi} x^{\\psi}) i k_{\\lambda}-\\pol2^{\\lambda}_{\\nu} \\exp(-i k_{\\psi} x^{\\psi}) i k_{\\lambda} -  1/2 \\pol1^{\\mu}_{\\mu} \\exp(i k_{\\psi} x^{\\psi}) i k_{\\nu} +  1/2 \\pol2^{\\mu}_{\\mu} \\exp(-i k_{\\psi} x^{\\psi}) i k_{\\nu} = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\mathbf{e}^{\\lambda}\\,_{\\nu} \\exp\\left(i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda}-\\bar{\\mathbf{e}}^{\\lambda}\\,_{\\nu} \\exp\\left(-i k_{\\psi} x^{\\psi}\\right) i k_{\\lambda} - \\frac{1}{2}\\mathbf{e}^{\\mu}\\,_{\\mu} \\exp\\left(i k_{\\psi} x^{\\psi}\\right) i k_{\\nu}+\\frac{1}{2}\\bar{\\mathbf{e}}^{\\mu}\\,_{\\mu} \\exp\\left(-i k_{\\psi} x^{\\psi}\\right) i k_{\\nu} = 0\\end{dmath*}"
				},
				{
					"cell_id": 5119035084508416635,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 12052884350320738997,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\exp(i k_{\\psi} x^{\\psi}) (\\pol1^{\\lambda}_{\\nu} i k_{\\lambda} -  1/2 \\pol1^{\\mu}_{\\mu} i k_{\\nu}) + \\exp(-i k_{\\psi} x^{\\psi}) (-\\pol2^{\\lambda}_{\\nu} i k_{\\lambda} +  1/2 \\pol2^{\\mu}_{\\mu} i k_{\\nu}) = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\exp\\left(i k_{\\psi} x^{\\psi}\\right) \\left(\\mathbf{e}^{\\lambda}\\,_{\\nu} i k_{\\lambda} - \\frac{1}{2}\\mathbf{e}^{\\mu}\\,_{\\mu} i k_{\\nu}\\right)+\\exp\\left(-i k_{\\psi} x^{\\psi}\\right) \\left(-\\bar{\\mathbf{e}}^{\\lambda}\\,_{\\nu} i k_{\\lambda}+\\frac{1}{2}\\bar{\\mathbf{e}}^{\\mu}\\,_{\\mu} i k_{\\nu}\\right) = 0\\end{dmath*}"
				}
			],
			"source": "substitute(wave_eq2, $\\partial_{\\mu}{x^{\\lambda}} -> \\delta_{\\mu}^{\\lambda}$);\neliminate_kronecker(wave_eq2);\nfactor_out(wave_eq2,$\\exp(i k_{\\psi} x^{\\psi}), \\exp(-i k_{\\psi} x^{\\psi})$);"
		},
		{
			"cell_id": 16120179897212495821,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 1344248271374021442,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "This equation must be true for every value of $\\mathbf{e}_{\\mu\\nu}$ and $\\bar{\\mathbf{e}}_{\\mu\\nu}$. Considering for \nexample $\\mathbf{e}_{\\mu\\nu}$, the second wave relation can be extracted using the \\algo{get_basis_component} function."
				}
			],
			"hidden": true,
			"source": "This equation must be true for every value of $\\mathbf{e}_{\\mu\\nu}$ and $\\bar{\\mathbf{e}}_{\\mu\\nu}$. Considering for \nexample $\\mathbf{e}_{\\mu\\nu}$, the second wave relation can be extracted using the \\algo{get_basis_component} function."
		},
		{
			"cell_id": 16727115536836309711,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 10214661859508094459,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 7959775877925069877,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "\\pol1^{\\lambda}_{\\nu} i k_{\\lambda} -  1/2 \\pol1^{\\mu}_{\\mu} i k_{\\nu} = 0"
						}
					],
					"source": "\\begin{dmath*}{}\\mathbf{e}^{\\lambda}\\,_{\\nu} i k_{\\lambda} - \\frac{1}{2}\\mathbf{e}^{\\mu}\\,_{\\mu} i k_{\\nu} = 0\\end{dmath*}"
				}
			],
			"source": "wave_eq2 = get_basis_component(wave_eq2,$\\exp(i k_{\\psi} x^{\\psi})$);"
		},
		{
			"cell_id": 813423569575838445,
			"cell_origin": "client",
			"cell_type": "input",
			"source": ""
		}
	],
	"description": "Cadabra JSON notebook format",
	"version": 1.0
}
