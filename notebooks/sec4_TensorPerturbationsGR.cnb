{
	"cell_id": 9918126328781195475,
	"cells": [
		{
			"cell_id": 5724826281235332937,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 16038329542537725452,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\part*{Tensor perturbations in General Relativity}\n\\author{Oscar Castillo-Felisola, Dominic T. Price and Mattia Scomparin}\n\\email{mattia.scompa@gmail.com}\n\nIn this notebook we derive the covariant expansion of the metric tensor in terms of tensor perturbations. \nWe extend this result to fundamental tensor objects in General Relativity (such as the Riemann Tensor, Ricci Tensor, ...)"
				}
			],
			"hidden": true,
			"source": "\\part*{Tensor perturbations in General Relativity}\n\\author{Oscar Castillo-Felisola, Dominic T. Price and Mattia Scomparin}\n\\email{mattia.scompa@gmail.com}\n\nIn this notebook we derive the covariant expansion of the metric tensor in terms of tensor perturbations. \nWe extend this result to fundamental tensor objects in General Relativity (such as the Riemann Tensor, Ricci Tensor, ...)"
		},
		{
			"cell_id": 443472510845603258,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 6773559919052960552,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Definitions and modules}\n\nFor this example we shall import the \\verb|header| and \\verb|perturbation| modules provided\nwith the bundle of notebooks. Other standard cadabra modules are imported by the \\verb|header| itself.\nWe then finish setting up the common environment by calling \\verb|init_properties| and defining the\nhighest order to which we will be performing our calculations."
				}
			],
			"hidden": true,
			"source": "\\section*{Definitions and modules}\n\nFor this example we shall import the \\verb|header| and \\verb|perturbation| modules provided\nwith the bundle of notebooks. Other standard cadabra modules are imported by the \\verb|header| itself.\nWe then finish setting up the common environment by calling \\verb|init_properties| and defining the\nhighest order to which we will be performing our calculations."
		},
		{
			"cell_id": 500919353740970048,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "from libraries.header import *\nfrom libraries.perturbations import *\nfrom libraries.config import maxPertOrder, pertLabel\n\ninit_properties(coordinates=$t,x,y,z$, metrics=[$g_{\\mu\\nu}$, $\\eta_{\\mu\\nu}$])"
		},
		{
			"cell_id": 4869042985325003059,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 11949787838627731425,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "We finish the initialisation by assigning weights to the objects that will appear in our perturbative expansion"
				}
			],
			"hidden": true,
			"source": "We finish the initialisation by assigning weights to the objects that will appear in our perturbative expansion"
		},
		{
			"cell_id": 17476728154354623729,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "\\eta{#}::Weight(label=pert,type=multiplicative,value=0).\n\\delta{#}::WeightInherit(label=pert,type=multiplicative,value=1).\n\\partial{#}::WeightInherit(label=pert,type=multiplicative,value=1)."
		},
		{
			"cell_id": 5122964105771745888,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 3051353011367060051,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "We now introduce are perturbative unit $h_{\\mu\\nu}$ which we will use to write the general\nmetric $g_{\\mu\\nu}$ as an expansion around the flat metric $\\eta_{\\mu\\nu}$"
				}
			],
			"hidden": true,
			"source": "We now introduce are perturbative unit $h_{\\mu\\nu}$ which we will use to write the general\nmetric $g_{\\mu\\nu}$ as an expansion around the flat metric $\\eta_{\\mu\\nu}$"
		},
		{
			"cell_id": 13584772221740958052,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "tensorPert:=h_{\\mu\\nu}:\n#Properties\n{h_{\\mu\\nu},h^{\\mu\\nu}}::Symmetric.\nh{#}::Depends(\\partial{#})."
		},
		{
			"cell_id": 6430284990854911045,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 10546981903283212775,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Contravariant Metric Perturbations $\\algo{gLow}\\,[\\;]$}\nWe now define an object \\verb|gLow| which contains the basic perturbative decompositions which for the basis for\nour calculations. The entries in the object represent the following:\n\\begin{description}\n\t\\item[\\texttt{unp}] \tThe unperturbed expression ($g_{\\mu\\nu}$)\n\t\\item[\\texttt{sym}] \tThe symbolic decomposition of the expression ($g_{\\mu\\nu} = \\,^{^{(0)}}g_{\\mu\\nu} + \\,^{^{(1)}}g_{\\mu\\nu} \n\t\t\t\t\t\t+ \\dots + \\,^{^{(n)}}g_{\\mu\\nu}$)\n\t\\item[\\texttt{ord}] \tThe perturbative orders of the expression collected into a list (so that the first element is\n\t\t\t\t\t\tthe zeroth order $\\,^{^{(0)}}g_{\\mu\\nu} = \\eta_{\\mu\\nu}$, and for $i>0$ the $i$th element is\n\t\t\t\t\t\t$\\,^{^{(i)}}g_{\\mu\\nu} = \\,^{^{(i)}}h_{\\mu\\nu}$)\n\\end{description}"
				}
			],
			"hidden": true,
			"source": "\\section*{Contravariant Metric Perturbations $\\algo{gLow}\\,[\\;]$}\nWe now define an object \\verb|gLow| which contains the basic perturbative decompositions which for the basis for\nour calculations. The entries in the object represent the following:\n\\begin{description}\n\t\\item[\\texttt{unp}] \tThe unperturbed expression ($g_{\\mu\\nu}$)\n\t\\item[\\texttt{sym}] \tThe symbolic decomposition of the expression ($g_{\\mu\\nu} = \\,^{^{(0)}}g_{\\mu\\nu} + \\,^{^{(1)}}g_{\\mu\\nu} \n\t\t\t\t\t\t+ \\dots + \\,^{^{(n)}}g_{\\mu\\nu}$)\n\t\\item[\\texttt{ord}] \tThe perturbative orders of the expression collected into a list (so that the first element is\n\t\t\t\t\t\tthe zeroth order $\\,^{^{(0)}}g_{\\mu\\nu} = \\eta_{\\mu\\nu}$, and for $i>0$ the $i$th element is\n\t\t\t\t\t\t$\\,^{^{(i)}}g_{\\mu\\nu} = \\,^{^{(i)}}h_{\\mu\\nu}$)\n\\end{description}"
		},
		{
			"cell_id": 10099723077719771319,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "gPertList = defPertList($g_{\\mu\\nu}$,pertLabel,maxPertOrder)\n\ngLow = {} \ngLow['unp'] = $g_{\\mu\\nu}$\ngLow['sym'] = defPertSum(gLow['unp'],gPertList) #Symbolic decomposition\ngLow['ord'] = defPertList($h_{\\mu\\nu}$,pertLabel,maxPertOrder) #Initialize (momentarily) ord as a pertList of h symbols\ngLow['ord'][0] = $\\eta_{\\mu\\nu}$\nfor i in range(maxPertOrder+1): \n\tl = gPertList[i]\n\tr = gLow['ord'][i]\n\tgLow['ord'][i] = $@(l) = @(r)$"
		},
		{
			"cell_id": 16003167697087734475,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 16146152531942694088,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 3758719181608272026,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "g_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}g_{\\mu \\nu}\\end{dmath*}"
				},
				{
					"cell_id": 12697086903151102508,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 18390823923654806058,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "g_{\\mu \\nu} = g0_{\\mu \\nu} + g1_{\\mu \\nu} + g2_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}g_{\\mu \\nu} = \\,^{^{(0)}}g_{\\mu \\nu}+\\,^{^{(1)}}g_{\\mu \\nu}+\\,^{^{(2)}}g_{\\mu \\nu}\\end{dmath*}"
				},
				{
					"cell_id": 2408655894820741100,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 5433340077650423762,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "g1_{\\mu \\nu} = h1_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(1)}}g_{\\mu \\nu} = \\,^{^{(1)}}h_{\\mu \\nu}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "# Check that the entries of gLow are what we expect\ngLow['unp'];\ngLow['sym'];\ngLow['ord'][1];"
		},
		{
			"cell_id": 1663593104444995200,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 6076041877415104214,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Covariant Metric Perturbations $\\algo{gUpp}\\,[\\;]$}\nSimilarly to above we define the \\verb|gUpp| object with entries \\verb|unp|, \\verb|sym| and \\verb|ord|. "
				}
			],
			"hidden": true,
			"source": "\\section*{Covariant Metric Perturbations $\\algo{gUpp}\\,[\\;]$}\nSimilarly to above we define the \\verb|gUpp| object with entries \\verb|unp|, \\verb|sym| and \\verb|ord|. "
		},
		{
			"cell_id": 11816267396145631225,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "gUpp = {} \ngUpp['unp'] = $g^{\\mu\\nu}$\ngUpp['sym'] = defPertSum(gUpp['unp'],defPertList(gUpp['unp'],pertLabel,maxPertOrder))\ngUpp['ord'] = [None]*(maxPertOrder+1) #Initialize (momentarily) ord as empty list of fixed positions (to use = instead .append)\ngUpp['ord'][0] = $g0^{\\mu\\nu}=\\eta^{\\mu\\nu}$"
		},
		{
			"cell_id": 8816310950490677591,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 5955934424481937716,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "Whereas for \\verb|gLow| the \\verb|ord| entry is a trivial definition as it is the object we are perturbing, when calculating the\n\\verb|ord| entry for $g^{\\mu\\nu}$ in terms of $\\,^{^{(i)}}h_{\\mu\\nu}$ we must use the identity"
				}
			],
			"hidden": true,
			"source": "Whereas for \\verb|gLow| the \\verb|ord| entry is a trivial definition as it is the object we are perturbing, when calculating the\n\\verb|ord| entry for $g^{\\mu\\nu}$ in terms of $\\,^{^{(i)}}h_{\\mu\\nu}$ we must use the identity"
		},
		{
			"cell_id": 8561913551631797808,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 7115556691625990833,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 6798546629760934652,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "0 = \\delta_{\\rho}^{\\nu}-g_{\\rho \\alpha} g^{\\alpha \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}0 = \\delta_{\\rho}\\,^{\\nu}-g_{\\rho \\alpha} g^{\\alpha \\nu}\\end{dmath*}"
				}
			],
			"source": "ident := 0 = \\delta_{\\rho}^{\\nu}-g_{\\rho\\alpha}g^{\\alpha\\nu};"
		},
		{
			"cell_id": 10746507777947715784,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 4414364873583159580,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "to calculate the entries. This identity can be written in terms of the perturbative expansions held in \n\\verb|gLow['sym']| and \\verb|gUpp['sym']|"
				}
			],
			"hidden": true,
			"source": "to calculate the entries. This identity can be written in terms of the perturbative expansions held in \n\\verb|gLow['sym']| and \\verb|gUpp['sym']|"
		},
		{
			"cell_id": 5722596021069094964,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 7269937119154941141,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 14667793501292618068,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "0 = \\delta_{\\rho}^{\\nu}-g0_{\\rho \\alpha} g0^{\\alpha \\nu}-g0_{\\rho \\alpha} g1^{\\alpha \\nu}-g0_{\\rho \\alpha} g2^{\\alpha \\nu}-g1_{\\rho \\alpha} g0^{\\alpha \\nu}-g1_{\\rho \\alpha} g1^{\\alpha \\nu}-g2_{\\rho \\alpha} g0^{\\alpha \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}0 = \\delta_{\\rho}\\,^{\\nu}-\\,^{^{(0)}}g_{\\rho \\alpha} \\,^{^{(0)}}g^{\\alpha \\nu}-\\,^{^{(0)}}g_{\\rho \\alpha} \\,^{^{(1)}}g^{\\alpha \\nu}-\\,^{^{(0)}}g_{\\rho \\alpha} \\,^{^{(2)}}g^{\\alpha \\nu}-\\,^{^{(1)}}g_{\\rho \\alpha} \\,^{^{(0)}}g^{\\alpha \\nu}-\\,^{^{(1)}}g_{\\rho \\alpha} \\,^{^{(1)}}g^{\\alpha \\nu}-\\,^{^{(2)}}g_{\\rho \\alpha} \\,^{^{(0)}}g^{\\alpha \\nu}\\end{dmath*}"
				}
			],
			"source": "subsPertSums(ident,pertLabel,maxPertOrder,gLow['sym'],gUpp['sym']);"
		},
		{
			"cell_id": 3013853640085001827,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 6196521711075436022,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "As $\\,^{^{(0)}}g_{\\mu\\nu} = \\eta_{\\mu\\nu}$ it is clear that \n$\\,^{^{(0)}}g_{\\rho\\alpha}\\,^{^{(0)}}g^{\\alpha\\nu} = \\delta_\\rho^{~\\nu}$\nand so we can find $\\,^{^{(i)}}g^{\\mu\\nu}$ by looking at the $i$th term in the expansion, \nwhich is what the function \\verb|getEquationpertOrder| from the \\verb|libraries.perturbations|\nnortebook does. Examining only terms of order $i$, the quantity $\\,^{^{(i)}}g^{\\alpha\\nu}$ can only be \naccompanied by the factor $\\,^{^{(0)}}g_{\\rho\\alpha}$ which is the flat metric. By contracting with\n$\\eta^{\\mu\\rho}$ we can isolate this quantity and then write it in terms of the lower order perturbations\nby substituting in for the values already calculated in \\verb|gLow['ord']| and \\verb|gUpp['ord']|."
				}
			],
			"hidden": true,
			"source": "As $\\,^{^{(0)}}g_{\\mu\\nu} = \\eta_{\\mu\\nu}$ it is clear that \n$\\,^{^{(0)}}g_{\\rho\\alpha}\\,^{^{(0)}}g^{\\alpha\\nu} = \\delta_\\rho^{~\\nu}$\nand so we can find $\\,^{^{(i)}}g^{\\mu\\nu}$ by looking at the $i$th term in the expansion, \nwhich is what the function \\verb|getEquationpertOrder| from the \\verb|libraries.perturbations|\nnortebook does. Examining only terms of order $i$, the quantity $\\,^{^{(i)}}g^{\\alpha\\nu}$ can only be \naccompanied by the factor $\\,^{^{(0)}}g_{\\rho\\alpha}$ which is the flat metric. By contracting with\n$\\eta^{\\mu\\rho}$ we can isolate this quantity and then write it in terms of the lower order perturbations\nby substituting in for the values already calculated in \\verb|gLow['ord']| and \\verb|gUpp['ord']|."
		},
		{
			"cell_id": 9698760317525433302,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "for i in range(1,maxPertOrder+1):\n\t# Select the ith order perturbation\n\torder_i = getEquationPertOrder(ident,pertLabel,i)\n\t# Multiply through by g0^{\\mu\\rho} and move gi^{\\alpha\\nu} to the other side\n\tmanip.multiply_through(order_i, $g0^{\\mu\\rho}$)\n\tdistribute(order_i)\n\tsubstitute(order_i, $g0^{\\mu\\rho}g0_{\\rho\\alpha}->\\delta^{\\mu}_{\\alpha}$)\n\tmanip.add_through(order_i, Ex(r'\\delta^{\\mu}_{\\alpha}'+r'g'+str(i)+r'^{\\alpha\\nu}'))\n\t\n\t# Substitute definitions from gLow['ord'] and gUpp['ord']\n\tfor j in range(0,i+1):\n\t\tsubstitute(order_i,gLow['ord'][j]) \n\tfor j in range(0,i):\t\n\t\tsubstitute(order_i,gUpp['ord'][j])\n\n\t# Simplify result and assign to gUpp['ord']\n\tdistribute(order_i)\n\teliminate_kronecker(order_i,repeat=True)\n\teliminate_metric(order_i,repeat=True)\n\tcanonicalise(order_i)\n\tgUpp['ord'][i] = order_i"
		},
		{
			"cell_id": 10980513630099000351,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 16093562417655170134,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 10358452068366698653,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "g^{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}g^{\\mu \\nu}\\end{dmath*}"
				},
				{
					"cell_id": 1750965379753572443,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 5287193541883423488,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "g^{\\mu \\nu} = g0^{\\mu \\nu} + g1^{\\mu \\nu} + g2^{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}g^{\\mu \\nu} = \\,^{^{(0)}}g^{\\mu \\nu}+\\,^{^{(1)}}g^{\\mu \\nu}+\\,^{^{(2)}}g^{\\mu \\nu}\\end{dmath*}"
				},
				{
					"cell_id": 12359526942088332855,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 10977096139905507507,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "g1^{\\mu \\nu} = -h1^{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(1)}}g^{\\mu \\nu} = -\\,^{^{(1)}}h^{\\mu \\nu}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "# Check that the entries of gUpp are what we expect\ngUpp['unp'];\ngUpp['sym'];\ngUpp['ord'][1];"
		},
		{
			"cell_id": 439694510925562061,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 2761401904668757222,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Generalized Perturbation Function}\nWe now have all the tools to generalise this procedure for an arbitrary tensor expression, which we\ndo in the following \\verb|perturb| function. The \\verb|perturb| function below takes an \\verb|Ex| object\nand produces dictionaries similar to \\verb|gUpp| and \\verb|gLow|. Note that the \\verb|ord| entry is only\ncalculated when the input \\verb|ex| is an equation."
				}
			],
			"hidden": true,
			"source": "\\section*{Generalized Perturbation Function}\nWe now have all the tools to generalise this procedure for an arbitrary tensor expression, which we\ndo in the following \\verb|perturb| function. The \\verb|perturb| function below takes an \\verb|Ex| object\nand produces dictionaries similar to \\verb|gUpp| and \\verb|gLow|. Note that the \\verb|ord| entry is only\ncalculated when the input \\verb|ex| is an equation."
		},
		{
			"cell_id": 2406685374866387059,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "def perturb(ex, pertDicts, pertLabel, maxPertOrder):\n\t# Initialize the pertDict object which will be returned\n\tpertDict = {}\n\tpertDict['ord'] = [None]*(maxPertOrder+1)\n\n\tif ex.top().name != r\"\\equals\":\n\t\t# Not an equation, 'unp' is just ex and 'sym' the result of defPertSum\n\t\tpertDict['unp'] = ex\n\t\tpertDict['sym'] = defPertSum(ex,defPertList(ex,pertLabel,maxPertOrder))\n\telse:\n\t\t# 'unp' and 'sym' are as above\n\t\tpertDict['unp'] = ex[0]\n\t\tpertDict['sym'] = defPertSum(ex[0],defPertList(ex[0],pertLabel,maxPertOrder))\n\n\t\t# If pertDicts are provided, use them to \n\t\tif pertDicts is not None:\n\t\t\t# Decompose pertDicts into two components for 'sym' and 'ord'\n\t\t\tsymPertList=[]\n\t\t\tordPertList=[]\n\t\t\tfor dic in pertDicts: \n\t\t\t\tsymPertList.append(dic['sym'])\n\t\t\t\tordPertList.append(dic['ord'])\n\n\t\t\t# Substitute symbolic decompositions\n\t\t\tsubbed_ex = subsPertSums($@(ex)$, pertLabel, maxPertOrder, *symPertList, pertDict['sym'])\n\n\t\t\t# Iterate over all orders substituting order decompositions\n\t\t\tfor i in range(0,maxPertOrder+1):\n\t\t\t\tcur_order = getEquationPertOrder(subbed_ex, pertLabel, i)\n\t\t\t\tfor dec in ordPertList: #All objects to substitute\n\t\t\t\t\tif dec[0] is not None: #leave symbolic the expressions\n\t\t\t\t\t\tfor j in range(i+1): #All orders up to i\n\t\t\t\t\t\t\tsubstitute(cur_order, dec[j], repeat=True)\n\n\t\t\t\t# Simplifications\n\t\t\t\tdistribute(cur_order)\n\t\t\t\tunwrap(cur_order,repeat=True)\n\t\t\t\tproduct_rule(cur_order)\n\t\t\t\teliminate_metric(cur_order,repeat=True)\n\t\t\t\teliminate_kronecker(cur_order,repeat=True)\n\t\t\t\tcanonicalise(cur_order)\n\n\t\t\t\t# Assign result of calculation to 'ord' \n\t\t\t\tpertDict['ord'][i] = cur_order\n\treturn pertDict"
		},
		{
			"cell_id": 17918446404613459374,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 8915363051102311865,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "To show the function of this algorithm we will apply it to the definition of the connection tensor given in the header file\n\\begin{equation}\n\t\\Gamma^{\\mu}_{\\nu\\tau}\t= \\frac{1}{2} g^{\\mu\\sigma} (\\partial_{\\tau}{g_{\\nu\\sigma}} \n\t\t\t\t\t\t+ \\partial_{\\nu}{g_{\\tau\\sigma}} \n\t\t\t\t\t\t- \\partial_{\\sigma}{g_{\\nu\\tau}})\n\\end{equation}\nand pass in the perturbative decompositions of $g_{\\mu\\nu}$ and $g^{\\mu\\nu}$ we have already calculated in \\verb|gLow| and\n\\verb|gUpp| respectively"
				}
			],
			"hidden": true,
			"source": "To show the function of this algorithm we will apply it to the definition of the connection tensor given in the header file\n\\begin{equation}\n\t\\Gamma^{\\mu}_{\\nu\\tau}\t= \\frac{1}{2} g^{\\mu\\sigma} (\\partial_{\\tau}{g_{\\nu\\sigma}} \n\t\t\t\t\t\t+ \\partial_{\\nu}{g_{\\tau\\sigma}} \n\t\t\t\t\t\t- \\partial_{\\sigma}{g_{\\nu\\tau}})\n\\end{equation}\nand pass in the perturbative decompositions of $g_{\\mu\\nu}$ and $g^{\\mu\\nu}$ we have already calculated in \\verb|gLow| and\n\\verb|gUpp| respectively"
		},
		{
			"cell_id": 13888907707583721393,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "connection = perturb(ch(), [gLow, gUpp], pertLabel, maxPertOrder)"
		},
		{
			"cell_id": 3641601324462586921,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 8345081910786250764,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 15967830114542865428,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "ch^{\\mu}_{\\nu \\tau}"
						}
					],
					"source": "\\begin{dmath*}{}\\Gamma^{\\mu}\\,_{\\nu \\tau}\\end{dmath*}"
				},
				{
					"cell_id": 15531234049115327667,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 3776716788096469069,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "ch^{\\mu}_{\\nu \\tau} = ch0^{\\mu}_{\\nu \\tau} + ch1^{\\mu}_{\\nu \\tau} + ch2^{\\mu}_{\\nu \\tau}"
						}
					],
					"source": "\\begin{dmath*}{}\\Gamma^{\\mu}\\,_{\\nu \\tau} = \\,^{^{(0)}}\\Gamma^{\\mu}\\,_{\\nu \\tau}+\\,^{^{(1)}}\\Gamma^{\\mu}\\,_{\\nu \\tau}+\\,^{^{(2)}}\\Gamma^{\\mu}\\,_{\\nu \\tau}\\end{dmath*}"
				},
				{
					"cell_id": 15149406323133956159,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 12409071463501563361,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "ch2^{\\mu}_{\\nu \\tau} =  1/2 \\partial_{\\tau}(h2_{\\nu}^{\\mu}) +  1/2 \\partial_{\\nu}(h2_{\\tau}^{\\mu}) -  1/2 \\partial^{\\mu}(h2_{\\nu \\tau}) -  1/2 h1^{\\mu \\sigma} \\partial_{\\tau}(h1_{\\nu \\sigma}) -  1/2 h1^{\\mu \\sigma} \\partial_{\\nu}(h1_{\\tau \\sigma}) +  1/2 h1^{\\mu \\sigma} \\partial_{\\sigma}(h1_{\\nu \\tau})"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(2)}}\\Gamma^{\\mu}\\,_{\\nu \\tau} = \\frac{1}{2}\\partial_{\\tau}{\\,^{^{(2)}}h_{\\nu}\\,^{\\mu}}+\\frac{1}{2}\\partial_{\\nu}{\\,^{^{(2)}}h_{\\tau}\\,^{\\mu}} - \\frac{1}{2}\\partial^{\\mu}{\\,^{^{(2)}}h_{\\nu \\tau}} - \\frac{1}{2}\\,^{^{(1)}}h^{\\mu \\sigma} \\partial_{\\tau}{\\,^{^{(1)}}h_{\\nu \\sigma}} - \\frac{1}{2}\\,^{^{(1)}}h^{\\mu \\sigma} \\partial_{\\nu}{\\,^{^{(1)}}h_{\\tau \\sigma}}+\\frac{1}{2}\\,^{^{(1)}}h^{\\mu \\sigma} \\partial_{\\sigma}{\\,^{^{(1)}}h_{\\nu \\tau}}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "connection['unp'];\nconnection['sym'];\nconnection['ord'][2];"
		},
		{
			"cell_id": 17889268409546327696,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 6956534399558487364,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "We will now continue to apply the \\verb|perturb| algorithm to the other objects which will be of interest throughout\nthe rest of this paper."
				}
			],
			"hidden": true,
			"source": "We will now continue to apply the \\verb|perturb| algorithm to the other objects which will be of interest throughout\nthe rest of this paper."
		},
		{
			"cell_id": 17273398531229571541,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 10034302219477985444,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Perturbations $\\algo{riemann}\\,[\\,\\,]$, $\\algo{ricciTensor}\\,[\\,\\,]$, $\\algo{ricciScalar}\\,[\\,\\,]$, $\\algo{einstein}\\,[\\,\\,]$}"
				}
			],
			"hidden": true,
			"source": "\\section*{Perturbations $\\algo{riemann}\\,[\\,\\,]$, $\\algo{ricciTensor}\\,[\\,\\,]$, $\\algo{ricciScalar}\\,[\\,\\,]$, $\\algo{einstein}\\,[\\,\\,]$}"
		},
		{
			"cell_id": 16759955390295493081,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "riemann = perturb(rm(),[connection],pertLabel,maxPertOrder)"
		},
		{
			"cell_id": 7172867014176416793,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "ricciTensor = perturb(rc(),[riemann],pertLabel,maxPertOrder)"
		},
		{
			"cell_id": 12632580790923891927,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "ricciScalar = perturb(rs(),[gUpp,ricciTensor],pertLabel,maxPertOrder)"
		},
		{
			"cell_id": 5726190052977102647,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "einstein = perturb(ei(),[gLow,ricciTensor,ricciScalar],pertLabel,maxPertOrder)"
		},
		{
			"cell_id": 18179311014264207147,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 2371460632502453938,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 1657641502372922819,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "rc_{\\sigma \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}R_{\\sigma \\nu}\\end{dmath*}"
				},
				{
					"cell_id": 4404224405509534028,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 12589635757320165591,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "rc_{\\sigma \\nu} = rc0_{\\sigma \\nu} + rc1_{\\sigma \\nu} + rc2_{\\sigma \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}R_{\\sigma \\nu} = \\,^{^{(0)}}R_{\\sigma \\nu}+\\,^{^{(1)}}R_{\\sigma \\nu}+\\,^{^{(2)}}R_{\\sigma \\nu}\\end{dmath*}"
				},
				{
					"cell_id": 9436251162881950564,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 6926318435504242733,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "rc1_{\\nu \\sigma} =  1/2 \\partial_{\\sigma \\tau}(h1_{\\nu}^{\\tau}) -  1/2 \\partial^{\\tau}_{\\tau}(h1_{\\nu \\sigma}) -  1/2 \\partial_{\\nu \\sigma}(h1_{\\tau}^{\\tau}) +  1/2 \\partial_{\\nu}^{\\tau}(h1_{\\sigma \\tau})"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(1)}}R_{\\nu \\sigma} = \\frac{1}{2}\\partial_{\\sigma \\tau}{\\,^{^{(1)}}h_{\\nu}\\,^{\\tau}} - \\frac{1}{2}\\partial^{\\tau}\\,_{\\tau}{\\,^{^{(1)}}h_{\\nu \\sigma}} - \\frac{1}{2}\\partial_{\\nu \\sigma}{\\,^{^{(1)}}h_{\\tau}\\,^{\\tau}}+\\frac{1}{2}\\partial_{\\nu}\\,^{\\tau}{\\,^{^{(1)}}h_{\\sigma \\tau}}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "ricciTensor['unp'];\nricciTensor['sym'];\nricciTensor['ord'][1];"
		},
		{
			"cell_id": 5013313708046740913,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 13326212464247211011,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Perturbations $\\algo{harmonicGauge}\\,[\\,\\,]$}"
				}
			],
			"hidden": true,
			"source": "\\section*{Perturbations $\\algo{harmonicGauge}\\,[\\,\\,]$}"
		},
		{
			"cell_id": 16232254088621042726,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 15574616542715206784,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 10246152229333292330,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "hg^{\\sigma} = g^{\\mu \\nu} ch^{\\sigma}_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}\\Gamma^{\\sigma} = g^{\\mu \\nu} \\Gamma^{\\sigma}\\,_{\\mu \\nu}\\end{dmath*}"
				}
			],
			"source": "hg{#}::LaTeXForm(\"\\Gamma\").\nhg := hg^{\\sigma} = g^{\\mu\\nu} ch^{\\sigma}_{\\mu\\nu};"
		},
		{
			"cell_id": 8054694890581511173,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "harmonicGauge = perturb(hg,[gUpp,connection],pertLabel,maxPertOrder)"
		},
		{
			"cell_id": 7686540887332740932,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 15328987673798126868,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 14150200583444706289,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "hg^{\\sigma}"
						}
					],
					"source": "\\begin{dmath*}{}\\Gamma^{\\sigma}\\end{dmath*}"
				},
				{
					"cell_id": 2695401479692051596,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 13921362093310698253,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "hg^{\\sigma} = hg0^{\\sigma} + hg1^{\\sigma} + hg2^{\\sigma}"
						}
					],
					"source": "\\begin{dmath*}{}\\Gamma^{\\sigma} = \\,^{^{(0)}}\\Gamma^{\\sigma}+\\,^{^{(1)}}\\Gamma^{\\sigma}+\\,^{^{(2)}}\\Gamma^{\\sigma}\\end{dmath*}"
				},
				{
					"cell_id": 1723648479329567038,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 4959395695670128570,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "hg2^{\\sigma} =  1/2 \\partial_{\\nu}(h2^{\\nu \\sigma}) +  1/2 \\partial^{\\nu}(h2_{\\nu}^{\\sigma}) -  1/2 \\partial^{\\sigma}(h2^{\\nu}_{\\nu}) -  1/2 h1^{\\sigma \\nu} \\partial_{\\rho}(h1^{\\rho}_{\\nu}) -  1/2 h1^{\\sigma \\nu} \\partial^{\\rho}(h1_{\\nu \\rho}) +  1/2 h1^{\\sigma \\nu} \\partial_{\\nu}(h1^{\\rho}_{\\rho}) -  1/2 h1^{\\mu \\nu} \\partial_{\\nu}(h1_{\\mu}^{\\sigma}) -  1/2 h1^{\\mu \\nu} \\partial_{\\mu}(h1_{\\nu}^{\\sigma}) +  1/2 h1^{\\mu \\nu} \\partial^{\\sigma}(h1_{\\mu \\nu})"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(2)}}\\Gamma^{\\sigma} = \\frac{1}{2}\\partial_{\\nu}{\\,^{^{(2)}}h^{\\nu \\sigma}}+\\frac{1}{2}\\partial^{\\nu}{\\,^{^{(2)}}h_{\\nu}\\,^{\\sigma}} - \\frac{1}{2}\\partial^{\\sigma}{\\,^{^{(2)}}h^{\\nu}\\,_{\\nu}} - \\frac{1}{2}\\,^{^{(1)}}h^{\\sigma \\nu} \\partial_{\\rho}{\\,^{^{(1)}}h^{\\rho}\\,_{\\nu}} - \\frac{1}{2}\\,^{^{(1)}}h^{\\sigma \\nu} \\partial^{\\rho}{\\,^{^{(1)}}h_{\\nu \\rho}}+\\frac{1}{2}\\,^{^{(1)}}h^{\\sigma \\nu} \\partial_{\\nu}{\\,^{^{(1)}}h^{\\rho}\\,_{\\rho}} - \\frac{1}{2}\\,^{^{(1)}}h^{\\mu \\nu} \\partial_{\\nu}{\\,^{^{(1)}}h_{\\mu}\\,^{\\sigma}} - \\frac{1}{2}\\,^{^{(1)}}h^{\\mu \\nu} \\partial_{\\mu}{\\,^{^{(1)}}h_{\\nu}\\,^{\\sigma}}+\\frac{1}{2}\\,^{^{(1)}}h^{\\mu \\nu} \\partial^{\\sigma}{\\,^{^{(1)}}h_{\\mu \\nu}}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "harmonicGauge['unp'];\nharmonicGauge['sym'];\nharmonicGauge['ord'][2];"
		},
		{
			"cell_id": 10645734583838980623,
			"cell_origin": "client",
			"cell_type": "latex",
			"cells": [
				{
					"cell_id": 1817094741093602726,
					"cell_origin": "client",
					"cell_type": "latex_view",
					"source": "\\section*{Perturbations $\\algo{matter}\\,[\\,\\,], \\algo{matterTr}\\,[\\,\\,], \\algo{matterSource}\\,[\\,\\,]$}"
				}
			],
			"hidden": true,
			"source": "\\section*{Perturbations $\\algo{matter}\\,[\\,\\,], \\algo{matterTr}\\,[\\,\\,], \\algo{matterSource}\\,[\\,\\,]$}"
		},
		{
			"cell_id": 15865598103118113619,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "mt:=T_{\\mu\\nu}:\ntr:=T=g^{\\mu\\nu}T_{\\mu\\nu}:\n#Properties\n{T_{\\mu\\nu},T_{\\mu\\nu}}::Symmetric.\nT{#}::Depends(\\partial{#})."
		},
		{
			"cell_id": 4508197717465907623,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "matter = perturb(mt,None,pertLabel,maxPertOrder)"
		},
		{
			"cell_id": 2833470496293492511,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 12411167904294368943,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 307090238003289942,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "T_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}T_{\\mu \\nu}\\end{dmath*}"
				},
				{
					"cell_id": 10645999810910398865,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 4501011765348283007,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "T_{\\mu \\nu} = T0_{\\mu \\nu} + T1_{\\mu \\nu} + T2_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}T_{\\mu \\nu} = \\,^{^{(0)}}T_{\\mu \\nu}+\\,^{^{(1)}}T_{\\mu \\nu}+\\,^{^{(2)}}T_{\\mu \\nu}\\end{dmath*}"
				},
				{
					"cell_id": 10089216937596352188,
					"cell_origin": "server",
					"cell_type": "verbatim",
					"source": "\\begin{verbatim}None\\end{verbatim}"
				}
			],
			"ignore_on_import": true,
			"source": "matter['unp'];\nmatter['sym'];\nmatter['ord'][2];"
		},
		{
			"cell_id": 16850619156465805138,
			"cell_origin": "client",
			"cell_type": "input",
			"source": "matterTr = perturb(tr,[gUpp,matter],pertLabel,maxPertOrder)"
		},
		{
			"cell_id": 3302867171733464717,
			"cell_origin": "client",
			"cell_type": "input",
			"cells": [
				{
					"cell_id": 10176000564922819171,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 2961770462747666075,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "T"
						}
					],
					"source": "\\begin{dmath*}{}T\\end{dmath*}"
				},
				{
					"cell_id": 12549948108955280020,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 11433399929445674203,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "T = T0 + T1 + T2"
						}
					],
					"source": "\\begin{dmath*}{}T = \\,^{^{(0)}}T+\\,^{^{(1)}}T+\\,^{^{(2)}}T\\end{dmath*}"
				},
				{
					"cell_id": 9746465226488069519,
					"cell_origin": "server",
					"cell_type": "latex_view",
					"cells": [
						{
							"cell_id": 7730864433152925787,
							"cell_origin": "server",
							"cell_type": "input_form",
							"source": "T2 = T2^{\\nu}_{\\nu}-h1^{\\mu \\nu} T1_{\\mu \\nu} + h1^{\\alpha}_{\\mu} h1^{\\mu \\nu} T0_{\\alpha \\nu}-h2^{\\mu \\nu} T0_{\\mu \\nu}"
						}
					],
					"source": "\\begin{dmath*}{}\\,^{^{(2)}}T = \\,^{^{(2)}}T^{\\nu}\\,_{\\nu}-\\,^{^{(1)}}h^{\\mu \\nu} \\,^{^{(1)}}T_{\\mu \\nu}+\\,^{^{(1)}}h^{\\alpha}\\,_{\\mu} \\,^{^{(1)}}h^{\\mu \\nu} \\,^{^{(0)}}T_{\\alpha \\nu}-\\,^{^{(2)}}h^{\\mu \\nu} \\,^{^{(0)}}T_{\\mu \\nu}\\end{dmath*}"
				}
			],
			"ignore_on_import": true,
			"source": "matterTr['unp'];\nmatterTr['sym'];\nmatterTr['ord'][2];"
		},
		{
			"cell_id": 6677714408648146009,
			"cell_origin": "client",
			"cell_type": "input",
			"source": ""
		}
	],
	"description": "Cadabra JSON notebook format",
	"version": 1.0
}
