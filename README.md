# Cadabra and Python algorithms in General Relativity and Cosmology II: Gravitational Waves

This is the official repository for the paper *Cadabra and Python algorithms in General Relativity and Cosmology II: Gravitational Waves*
containing the set of accompanying notebooks and libraries which are discussed throughout the article.

## Usage

### Prerequisites

In order to run the notebooks in this repository, you will need a recent version of Cadabra installed on your system. The notebooks
make use of the new `join` functionality introduced in
[40f334a](https://github.com/kpeeters/cadabra2/commit/751af2ce0427fbd473dd4ecfba4489fbac88e855)
and some outputs contain LaTeX macros which were introduced in
[0ade99f](https://github.com/kpeeters/cadabra2/commit/0ade99fb6b7c5e55147b3d3d3764241d13db1f46). The last stable version which
the notebooks have been tested on is
[5b5c7f0](https://github.com/kpeeters/cadabra2/commit/5b5c7f047e77556bde80af0769a0f09ae31b6802)
which was released on the 14th September 2022. Installation instructions for various operating systems can be found in the
[official cadabra2 repository](https://github.com/kpeeters/cadabra2).

In order to make the notebooks in this repository available locally on your machine, you will need to either:

- Have [git](https://git-scm.com/) installed on your system, and then run `git clone https://gitlab.com/cdbgr/cadabra-gravity-II` 
- Click on the download symbol above, select an archive format to download the repository as and extract it into a location on your computer

### Running the notebooks

The notebooks are intended to be run from within the cadabra2 GTK interface. The notebooks are named `secX_TITLE.cnb` where
`X` is the number of the section which the notebook is based on and `TITLE` is a descriptive name about the main focus
of the notebook.

People unfamiliar with cadabra are recommended to follow through some of the introductory tutorials which can be found on the
[cadabra website](https://cadabra.science/tutorials.html) or the cadabra tutorial written by 
[Leo Brewin](https://github.com/leo-brewin/cadabra-tutorial), although the article does not assume that the reader is a
proficient user and many key concepts are explained.

## Authors

- [Oscar Castillo-Felisola](https://gitlab.com/Doxdrum)
- [Dominic T. Price](https://gitlab.com/dominicprice)
- [Mattia Scomparin](https://gitlab.com/TIAPHYSICS)
